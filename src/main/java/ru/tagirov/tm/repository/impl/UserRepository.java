package ru.tagirov.tm.repository.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.repository.AbstractRepository;
import ru.tagirov.tm.api.repository.IUserRepository;

@RequiredArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository<User> {

    @Override
    public @Nullable User findOneByLogin(@NotNull String login) {
        for (User user : map.values()) {
            if(user.getLogin().equals(login))
                return user;
        }
        return null;
    }
}
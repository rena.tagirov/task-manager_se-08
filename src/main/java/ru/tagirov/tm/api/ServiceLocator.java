package ru.tagirov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.terminal.TerminalService;

import java.util.Collection;

public interface ServiceLocator {

    @NotNull
    IProjectService<Project> getIProjectService();

    @NotNull
    ITaskService<Task> getITaskService();

    @NotNull
    IUserService<User> getIUserService();

    @NotNull
    Collection<AbstractCommand> getCommands();


    @NotNull
    TerminalService getTerminalService();

}

package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IRepository<T extends AbstractEntity> {

    void persist(@NotNull final T t);

    @Nullable
    T merge(@NotNull final T t);

    @Nullable
    T findOne(@NotNull final String uuid);

    @NotNull
    Collection<T> findAll();

    @Nullable
    T remove(@NotNull final String uuid);

    void removeAll();

}
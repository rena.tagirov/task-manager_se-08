package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IService<T> {

    void persist(@NotNull final T t);

    @Nullable
    T merge(@NotNull final T t);

    @Nullable
    T findOne(@NotNull final T t);

    @NotNull
    Collection<T> findAll();

    @Nullable
    T remove(@NotNull final T t);

    void removeAll();

}
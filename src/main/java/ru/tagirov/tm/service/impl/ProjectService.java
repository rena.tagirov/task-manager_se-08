package ru.tagirov.tm.service.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.repository.IProjectRepository;
import ru.tagirov.tm.service.AbstractService;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.service.PurposeService;

@RequiredArgsConstructor
public class ProjectService extends PurposeService<Project> implements IProjectService<Project> {

    public ProjectService(@NotNull final IProjectRepository<Project> purposeRepository) {
        super(purposeRepository);
    }

}


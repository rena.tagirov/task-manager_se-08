package ru.tagirov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.api.repository.IUserRepository;
import ru.tagirov.tm.service.AbstractService;
import ru.tagirov.tm.api.service.IUserService;

public class UserService extends AbstractService<User> implements IUserService<User> {

    @NotNull
    private IUserRepository iUserRepository;

    @Nullable
    private User user = null;

    public UserService(@NotNull final IUserRepository<User> abstractRepository) {
        super(abstractRepository);
        iUserRepository = abstractRepository;
    }

    @Nullable
    public User getCurrentUser() {
        return user;
    }

    public void setCurrentUser(@Nullable final User user) {
        this.user = user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull String login) {
        return iUserRepository.findOneByLogin(login);
    }

}

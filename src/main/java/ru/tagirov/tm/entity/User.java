package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.Role;

import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
public class User extends AbstractEntity{

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private Role role;

    public User(@NonNull final String id,
                @NonNull final String login,
                @NonNull final String password,
                @NonNull final Role role) {
        super(id);
        this.login = login;
        this.password = password;
        this.role = role;
    }
}

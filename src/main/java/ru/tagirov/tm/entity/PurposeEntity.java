package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class PurposeEntity extends AbstractEntity{

    @NonNull
    private String id;

    @NonNull
    private String name;
    @NonNull
    private String description;

    @NonNull
    private Date dateCreate;

    @Nullable
    private Date dateUpdate;

    @NonNull
    private String userId;


    public PurposeEntity(@NotNull final String id,
                         @NotNull final String name,
                         @NotNull final String description,
                         @NotNull final Date dateCreate,
                         @NotNull final Date dateUpdate,
                         @NotNull final String userId) {
        super(id);
        this.name = name;
        this.description = description;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.userId = userId;
    }

}

package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class Task extends PurposeEntity{

    @Nullable
    private String idProject;

    public Task(@NonNull final String id,
                @NonNull final String name,
                @NonNull final String description,
                @NonNull final Date dateCreate,
                @Nullable final Date dateUpdate,
                @NonNull final String idProject,
                @NonNull final String userId) {
        super(id, name, description, dateCreate, dateUpdate, userId);
        this.idProject = idProject;

    }
}
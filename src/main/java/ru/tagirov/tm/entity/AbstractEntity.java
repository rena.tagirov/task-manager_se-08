package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEntity {

    @NotNull
    private String id;

    public AbstractEntity(@NotNull final String id){
        this.id = id;
    }
}

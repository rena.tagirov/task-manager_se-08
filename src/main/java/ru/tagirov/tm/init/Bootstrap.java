package ru.tagirov.tm.init;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.api.repository.IProjectRepository;
import ru.tagirov.tm.api.repository.ITaskRepository;
import ru.tagirov.tm.api.repository.IUserRepository;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.repository.impl.ProjectRepository;
import ru.tagirov.tm.repository.impl.TaskRepository;
import ru.tagirov.tm.repository.impl.UserRepository;
import ru.tagirov.tm.service.impl.ProjectService;
import ru.tagirov.tm.service.impl.TaskService;
import ru.tagirov.tm.service.impl.UserService;
import ru.tagirov.tm.terminal.TerminalService;
import ru.tagirov.tm.util.Md5Util;

import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final IProjectRepository<Project> projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository<Task> taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository<User> userRepository = new UserRepository();

    @NotNull
    private final IProjectService<Project> projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService<Task> taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserService<User> userService = new UserService(userRepository);

    @NotNull
    private final Set<Class<? extends AbstractCommand>> commandClasses = getAbstractCommandClasses();

    private void registry(@NotNull final AbstractCommand command) {
        @NotNull final String cliCommand = command.command();

        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void init() throws IllegalAccessException, InstantiationException, NoSuchAlgorithmException {
        for (@NotNull final Class<? extends AbstractCommand> abstractClass : commandClasses) {
            registry(abstractClass.newInstance());
        }
        @NotNull final User manager = new User(UUID.randomUUID().toString(), "m", Md5Util.getHash("m"), Role.MANAGER);
        @NotNull final User admin = new User(UUID.randomUUID().toString(), "a", Md5Util.getHash("a"), Role.ADMIN);
        userService.persist(manager);
        userService.persist(admin);
    }

    private void execute(@NotNull final String command) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("No such command!");
            return;
        }
        if (userService.getCurrentUser() == null && !abstractCommand.isSecure()) {
            abstractCommand.execute();
            return;
        }
        if (userService.getCurrentUser() != null && abstractCommand.isSecure() && (abstractCommand.getRole().getLvl() <= userService.getCurrentUser().getRole().getLvl())) {
            abstractCommand.execute();
            return;
        }
        if ("help".equals(abstractCommand.command()) || "about".equals(abstractCommand.command())) {
            abstractCommand.execute();
            return;
        }
        System.out.println("No such command!");
    }

    private Set<Class<? extends AbstractCommand>> getAbstractCommandClasses() {
        return new Reflections("ru.tagirov.tm").getSubTypesOf(AbstractCommand.class);
    }

    public void start() throws Exception {
        init();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("\"help\" - get access command.");
        @Nullable String command;

        while (true) {
            command = getTerminalService().readLine();
            if ("exit".equals(command))
                return;
            try {
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    public TerminalService getTerminalService() {
        return new TerminalService();
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public IProjectService<Project> getIProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService<Task> getITaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService<User> getIUserService() {
        return userService;
    }
}
package ru.tagirov.tm.terminal;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class TerminalService {

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NonNull
    public String readLine() throws IOException {
        return reader.readLine();
    }

    public int parseInt() throws IOException {
        return Integer.parseInt(reader.readLine());
    }

    public void printProject(final Project project){
        System.out.println(new StringBuilder()
                .append("\nName: ").append(project.getName())
                .append("\nDescription: ").append(project.getDescription())
                .append("\nStart date: ").append(project.getDateCreate())
                .append("\nEnd date: ").append(project.getDateUpdate())
                .append("\n--------------------------------------------------"));
    }

    public void printTask(@NotNull final String projectName, @NotNull final Task task) {
        System.out.println(new StringBuilder()
                .append("\nProject Name: ").append(projectName)
                .append("\nName: ").append(task.getName())
                .append("\nDescription: ").append(task.getDescription())
                .append("\nStart date: ").append(DateUtil.getDate(task.getDateCreate()))
                .append("\nEnd date: ").append(DateUtil.getDate(task.getDateUpdate()))
                .append("\n--------------------------------------------------"));
    }

    public void printTask(@NotNull final Task task) {
        System.out.println(new StringBuilder()
                .append("\nName: ").append(task.getName())
                .append("\nDescription: ").append(task.getDescription())
                .append("\nStart date: ").append(DateUtil.getDate(task.getDateCreate()))
                .append("\nEnd date: ").append(DateUtil.getDate(task.getDateUpdate()))
                .append("\n--------------------------------------------------"));
    }

    public void printUser(@NotNull final User user) {
        System.out.println(new StringBuilder()
                .append(user.getLogin())
                .append("\nRole: ")
                .append(user.getRole().getRole())
                .append("\n--------------------------------------------------"));
    }

    public void printRoleField(){
        System.out.println(new StringBuilder().append("ENTER ROLE:")
                .append("\n    1. admin")
                .append("\n    2. manager")
                .append("\n    3. user"));
    }

    public void printProjects(@NotNull final List<Project> projects) {
        int count = 0;
        for (Project project : projects) {
            System.out.println(++count + ". " + project.getName());
        }
    }

    public void printTasks(@NotNull final List<Task> tasks) {
        int count = 0;
        for (Task task : tasks) {
            System.out.println(++count + ". " + task.getName());
        }
    }

    public void printProjectField() {
        System.out.println(new StringBuilder().append("What do you want to change? ")
                .append("\n 1. Name")
                .append("\n 2. Description")
                .append("\n 3. Start date")
                .append("\n 4. End date")
                .append("\n 5. Status"));

    }

    public void printTaskField() {
        System.out.println(new StringBuilder().append("What do you want to change? ")
                .append("\n 1. Name")
                .append("\n 2. Description")
                .append("\n 3. Start date")
                .append("\n 4. End date")
                .append("\n 5. Status")
                .append("\n 6. Project"));

    }

    public void printStatusField(){
        System.out.println(new StringBuilder().append("Select status:")
                .append("\n 1.planned")
                .append("\n 2.during")
                .append("\n 3.ready"));
    }

}

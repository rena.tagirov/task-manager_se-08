package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project clear";
    }

    @Override
    public @NonNull String description() {
        return "delete all projects";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() {

        @NotNull final IProjectService<Project> projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService<Task> taskService = serviceLocator.getITaskService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        for (@NotNull final Project project : projects) {
            taskService.removeAllByProjectId(user.getId(), project.getId());
        }

        projectService.removeAll(user.getId());
        System.out.println("[ALL PROJECT REMOVE]");
    }
}

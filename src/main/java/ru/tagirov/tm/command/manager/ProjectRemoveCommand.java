package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project remove";
    }

    @Override
    public @NonNull String description() {
        return "delete one project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws IOException {

        @NotNull final IProjectService<Project> projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService<Task> taskService = serviceLocator.getITaskService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        System.out.println("[PROJECT REMOVE]");
        serviceLocator.getTerminalService().printProjects(projects);

        System.out.println("ENTER NUMBER:[0-Non-Project]");
        final int number = serviceLocator.getTerminalService().parseInt();

        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return;
        }

        if (number == 0) {
            System.out.println("Non-project tasks remove");
            taskService.removeAllByProjectId(user.getId(), null);
            System.out.println("[OK]");
            return;
        }

        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i) {
                @NotNull final Project project = Objects.requireNonNull(projectService.remove(user, projects.get(i)));
                taskService.removeAllByProjectId(user.getId(), project.getId());
            }
        }

        System.out.println("[OK]");
    }
}

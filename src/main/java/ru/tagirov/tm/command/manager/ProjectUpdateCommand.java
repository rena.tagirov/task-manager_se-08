package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project update";
    }

    @Override
    public @NonNull String description() {
        return "change project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws IOException, ParseException {

        @NotNull final IProjectService<Project> projectService = serviceLocator.getIProjectService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        System.out.println("[PROJECT UPDATE]");
        serviceLocator.getTerminalService().printProjects(projects);

        System.out.println("ENTER NUMBER:");
        final int number = serviceLocator.getTerminalService().parseInt();

        if (number < 1 || number > projects.size()) {
            System.out.println("ENTER CORRECT NUMBER!");
            return;
        }

        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i) {
                projectService.merge(selectField(projects.get(i)));
                break;
            }
        }

        System.out.println("[OK]");
    }

    @NotNull
    private Project selectField(@NotNull final Project project) throws ParseException, IOException {
        serviceLocator.getTerminalService().printProjectField();

        @NotNull final String fieldNumber = serviceLocator.getTerminalService().readLine();
        switch (fieldNumber) {
            case "1":
                System.out.println("Replace name to:");
                project.setName(serviceLocator.getTerminalService().readLine());
                break;
            case "2":
                System.out.println("Replace description to:");
                project.setDescription(serviceLocator.getTerminalService().readLine());
                break;
            case "3":
                System.out.println("Replace start date to:");
                project.setDateCreate(DateUtil.getDate(serviceLocator.getTerminalService().readLine()));
                break;
            case "4":
                System.out.println("Replace end date to:");
                project.setDateUpdate(DateUtil.getDate(serviceLocator.getTerminalService().readLine()));
                break;
            default:
                System.out.println("Nothing has changed!");
                break;
        }

        return project;
    }
}

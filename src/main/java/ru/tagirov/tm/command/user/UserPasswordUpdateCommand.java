package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.Md5Util;
import ru.tagirov.tm.terminal.TerminalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class UserPasswordUpdateCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "password update";
    }

    @Override
    public @NonNull String description() {
        return "update you password";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, NoSuchAlgorithmException {

        @NotNull final IUserService<User> userService = serviceLocator.getIUserService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());

        System.out.println("[USER PASSWORD UPDATE]");
        System.out.println("[ENTER OLD PASSWORD:]");
        @NotNull final String oldPass = serviceLocator.getTerminalService().readLine();
        if (!user.getPassword().equals(Md5Util.getHash(oldPass))) {
            System.out.println("ENTER CORRECT PASSWORD!");
            return;
        }
        System.out.println("PASSWORD CORRECT!");
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull final String newPass = serviceLocator.getTerminalService().readLine();
        @NotNull final String newPassHash = Md5Util.getHash(newPass);

        user.setPassword(newPassHash);
        userService.merge(user);
        System.out.println(user.getLogin() + " PASSWORD IS CHANGED!");
    }
}

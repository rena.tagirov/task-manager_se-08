package ru.tagirov.tm.command.user;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskListCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "task list";
    }

    @Override
    public @NonNull String description() {
        return "see all tasks";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {

        @NotNull final IProjectService<Project> projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService<Task> taskService = serviceLocator.getITaskService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());

        System.out.println("[TASK LIST]");
        @NotNull final List<Task> tasks = new ArrayList<>(taskService.findAllByUserId(user.getId()));
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));
        for (int i = 0; i < tasks.size(); i++) {
            @NotNull final Task task = tasks.get(i);
            @NotNull String projectName = "Non-project";

            for (@NotNull final Project project : projects) {
                if (project.getId().equals(task.getIdProject())) {
                    projectName = project.getName();
                }
            }
            System.out.println("Task #" + (i + 1) + ".");
            serviceLocator.getTerminalService().printTask(projectName, task);
        }
    }

}

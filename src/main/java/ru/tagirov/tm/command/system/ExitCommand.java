package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;

public class ExitCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "exit";
    }

    @Override
    public @NonNull String description() {
        return "exit with you account";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {

        System.out.println("[USER LOG OUT]");
        serviceLocator.getIUserService().setCurrentUser(null);
        System.out.println("[OK]");
    }
}
package ru.tagirov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;

public class AboutCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "about";
    }

    @Override
    public @NonNull String description() {
        return "program description";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Build Number: " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));
    }
}

package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.Md5Util;
import ru.tagirov.tm.terminal.TerminalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class LoginCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "login";
    }

    @Override
    public @NonNull String description() {
        return "log into your account";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, NoSuchAlgorithmException {

        @NotNull final IUserService<User> userService = serviceLocator.getIUserService();

        System.out.println("[USER AUTH]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().readLine();

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = serviceLocator.getTerminalService().readLine();

        @NotNull final String passwordHash = Md5Util.getHash(password);

        @NotNull final List<User> users = new ArrayList<>(userService.findAll());

        for (@NotNull final User user : users) {
            if (user.getLogin().equals(login) && user.getPassword().equals(passwordHash)) {
                serviceLocator.getIUserService().setCurrentUser(user);
                System.out.println("[OK]");
                return;
            }
        }

        System.out.println("Incorrect user or password!");
    }

}

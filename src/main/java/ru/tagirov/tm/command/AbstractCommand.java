package ru.tagirov.tm.command;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.ICommand;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.api.ServiceLocator;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public abstract class AbstractCommand implements ICommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NonNull
    public abstract String command();

    @NonNull
    public abstract String description();

    public abstract void execute() throws IOException, ParseException, NoSuchAlgorithmException;

    public abstract boolean isSecure();

    @NotNull
    public abstract Role getRole();
}

package ru.tagirov.tm.util;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RequiredArgsConstructor
public class DateUtil {

    @NotNull
    public static Date getDate(@NotNull String dates) throws ParseException {
        return new SimpleDateFormat("dd-mm-yyyy").parse(dates);
    }

    @NotNull
    public static String getDate(@NotNull final Date date) {
        return new SimpleDateFormat("dd-mm-yyyy").format(date);
    }
}